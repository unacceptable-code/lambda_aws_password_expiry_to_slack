terraform {
    backend "s3" {
        bucket  = "scriptmyjob.terraform.tfstate"
        key     = "Password_Expiry/terraform.tfstate"
        region  = "us-west-2"
        encrypt = "true"
        profile = "default"
    }
}
